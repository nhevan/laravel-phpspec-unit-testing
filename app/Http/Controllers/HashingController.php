<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Contracts\Hashing\Hasher;

class HashingController extends Controller
{
	protected $hasher;
	protected $request;

	public function __construct(Hasher $hasher, Request $request)
	{
		$this->hasher = $hasher;
		$this->request = $request;
	}



    public function index()
    {
    	return view('hash.index');
    }
    public function postIndex()
    {
    	$hashedPassword = $this->hasher->make($this->request->input('password'));

    	return "Your hashed password is {$hashedPassword}.";
    }
}
