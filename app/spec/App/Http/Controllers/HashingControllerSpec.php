<?php

namespace spec\App\Http\Controllers;

use Prophecy\Argument;
use PhpSpec\ObjectBehavior;
use Illuminate\Http\Request;
use Illuminate\Contracts\Hashing\Hasher;
use App\Http\Controllers\HashingController;

class HashingControllerSpec extends ObjectBehavior
{
	function let(Hasher $hasher, Request $request){
		$this->beConstructedWith($hasher, $request);
	}
    function it_is_initializable()
    {
        $this->shouldHaveType(HashingController::class);
    }
    function it_hashes_a_provided_password(Hasher $hasher, Request $request)
    {
    	$request->input('password')->shouldBeCalled();
    	$hasher->make(Argument::any())->shouldBeCalled()->willReturn('tmp');
    	$this->postIndex()->shouldReturn('Your hashed password is tmp.');
    }
}
