<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{

    /** @test **/
    public function it_hashes_a_provided_password()
    {
        //arrange
        Hash::shouldReceive('make')->once()->andReturn('foobar_hashed_password');
        $response = $this->action('POST', 'HashingController@postIndex', ['password' => 'hello']);
    
        //act
        
    
        //assert
        $this->assertEquals('Your hashed password is foobar_hashed_password.', $response->getContent());
    }
}
